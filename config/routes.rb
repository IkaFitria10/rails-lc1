Rails.application.routes.draw do
  resources :Student
  resources :Exam
  resources :Teacher
  resources :Report
  resources :Payment
  resources :Pocket
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
