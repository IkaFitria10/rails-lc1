class CreateReports < ActiveRecord::Migration[6.0]
  def change
    create_table :reports do |t|
      t.string :title
      t.float :hasil
      t.string :mapel
      t.string :teacher_id
      t.string :student_id
      t.string :date

      t.timestamps
    end
  end
end
