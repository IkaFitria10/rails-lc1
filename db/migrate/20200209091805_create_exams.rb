class CreateExams < ActiveRecord::Migration[6.0]
  def change
    create_table :exams do |t|
      t.string :title
      t.string :mapel
      t.integer :duration
      t.float :nilai
      t.string :aktivation
      t.integer :level
      t.string :student_id

      t.timestamps
    end
  end
end
