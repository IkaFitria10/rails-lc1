require 'test_helper'

class PocketControllerTest < ActionDispatch::IntegrationTest
  test "should get demo" do
    get pocket_demo_url
    assert_response :success
  end

  test "should get index" do
    get pocket_index_url
    assert_response :success
  end

end
